-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 26, 2021 at 11:49 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `batallanaval`
--

-- --------------------------------------------------------

--
-- Table structure for table `tableros`
--

CREATE TABLE `tableros` (
  `id` int(10) NOT NULL,
  `codigo` varchar(10) NOT NULL,
  `usuario1_id` int(10) NOT NULL,
  `usuario2_id` int(10) NOT NULL,
  `estatus` varchar(15) NOT NULL,
  `ganador_id` int(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tableros`
--

INSERT INTO `tableros` (`id`, `codigo`, `usuario1_id`, `usuario2_id`, `estatus`, `ganador_id`, `created_at`, `updated_at`) VALUES
(12, 'hDbcJ', 19, 20, 'jugando', 19, '2021-03-26 21:50:31', '2021-03-27 03:50:31'),
(13, 'I4885', 19, 20, 'finalizado', 19, '2021-03-26 22:48:28', '2021-03-27 04:37:15');

-- --------------------------------------------------------

--
-- Table structure for table `tablero_barcos`
--

CREATE TABLE `tablero_barcos` (
  `id` int(10) NOT NULL,
  `tablero_id` int(10) NOT NULL,
  `usuario_id` int(10) NOT NULL,
  `barco1` int(10) NOT NULL,
  `barco2` int(10) NOT NULL,
  `barco3` int(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tablero_barcos`
--

INSERT INTO `tablero_barcos` (`id`, `tablero_id`, `usuario_id`, `barco1`, `barco2`, `barco3`, `created_at`, `updated_at`) VALUES
(13, 12, 19, 1, 5, 9, '2021-03-27 03:50:17', '2021-03-27 03:50:17'),
(14, 12, 20, 12, 11, 10, '2021-03-27 03:50:31', '2021-03-27 03:50:31'),
(15, 13, 19, 1, 5, 9, '2021-03-27 04:37:02', '2021-03-27 04:37:02'),
(16, 13, 20, 3, 6, 9, '2021-03-27 04:37:14', '2021-03-27 04:37:14');

-- --------------------------------------------------------

--
-- Table structure for table `tablero_movimiento`
--

CREATE TABLE `tablero_movimiento` (
  `id` int(10) NOT NULL,
  `tablero_id` int(10) NOT NULL,
  `usuario_id` int(10) NOT NULL,
  `posicion` int(10) NOT NULL,
  `estatus` varchar(15) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tablero_movimiento`
--

INSERT INTO `tablero_movimiento` (`id`, `tablero_id`, `usuario_id`, `posicion`, `estatus`, `created_at`, `updated_at`) VALUES
(41, 12, 19, 3, '0', '2021-03-27 03:50:48', '2021-03-27 03:50:48'),
(42, 12, 20, 10, '0', '2021-03-27 03:56:20', '2021-03-27 03:56:20'),
(43, 13, 19, 3, '1', '2021-03-27 04:37:38', '2021-03-27 04:37:38'),
(44, 13, 20, 1, '1', '2021-03-27 04:37:46', '2021-03-27 04:37:46'),
(45, 13, 19, 6, '1', '2021-03-27 04:37:54', '2021-03-27 04:37:54'),
(46, 13, 20, 7, '0', '2021-03-27 04:38:01', '2021-03-27 04:38:01'),
(47, 13, 19, 9, '1', '2021-03-27 04:38:08', '2021-03-27 04:38:08');

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(10) NOT NULL,
  `correo` varchar(70) NOT NULL,
  `password` varchar(255) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`id`, `correo`, `password`, `updated_at`, `created_at`) VALUES
(13, 'danielagodma@gmail.com', '$2y$10$4FZ8T..AvYJQDlesBJcA3ugD5Hq3v48Za5S6nuO09LkaNWxPAoCkW', '2021-03-16 12:23:38', '2021-03-16 12:23:38'),
(14, 'josuejosuebibi@gmail.com', '$2y$10$Zf9njMkvpeFdmBR0s/h3/O2F9ImKONuuBs48pLoeYLupNwK.XLQmO', '2021-03-17 10:24:45', '2021-03-17 10:24:45'),
(15, 'juan@gmail.com', '$2y$10$sQqpwrZyxHprZfLdz/XtQ.OLV/uX7QxejJb95vxhP15V3igShiEoe', '2021-03-18 00:46:13', '2021-03-18 00:46:13'),
(16, 'daniela_1319104670@uptecamac.edu.mx', '$2y$10$fdT9NNmNRuOWcIPZOYhAoOx3/OsFFjBiSxiZBfsaSNgPY4TfAcDtq', '2021-03-18 06:44:24', '2021-03-18 06:44:24'),
(17, 'impresosideales@hotmail.com', '$2y$10$g4nkfB.wpjbwGrWL2Cqi6.EY0XgxS0q/sDSTcYj2Qn0oXMWUNFOIa', '2021-03-18 13:37:51', '2021-03-18 13:37:51'),
(18, 'daniela@gmail.com', '$2y$10$Kf.oIJmNxNvPY53QDevEGuuK/y9HVKB3MeJnyLmuiVTXmmVi9uA62', '2021-03-23 04:39:33', '2021-03-23 04:39:33'),
(19, 'sorrow2287@gmail.com', '$2y$10$zzqRnRNuMDWSavxhHCjQReyimmAQrgQNlhndIIgAqLO3cZYwXOZN.', '2021-03-23 04:47:10', '2021-03-23 04:47:10'),
(20, 'usuario@correo.com', '$2y$10$nupDUL.Rdw7CDNKWGdNUe.IjaiOI8ZPY.T6d0NTqCC3T.2miJah3q', '2021-03-23 04:47:23', '2021-03-23 04:47:23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tableros`
--
ALTER TABLE `tableros`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tablero_barcos`
--
ALTER TABLE `tablero_barcos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `usuario_id` (`usuario_id`),
  ADD KEY `tablero_id` (`tablero_id`);

--
-- Indexes for table `tablero_movimiento`
--
ALTER TABLE `tablero_movimiento`
  ADD PRIMARY KEY (`id`),
  ADD KEY `usuarioId` (`usuario_id`),
  ADD KEY `tableroId` (`tablero_id`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tableros`
--
ALTER TABLE `tableros`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tablero_barcos`
--
ALTER TABLE `tablero_barcos`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tablero_movimiento`
--
ALTER TABLE `tablero_movimiento`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tablero_barcos`
--
ALTER TABLE `tablero_barcos`
  ADD CONSTRAINT `tablero_id` FOREIGN KEY (`tablero_id`) REFERENCES `tableros` (`id`),
  ADD CONSTRAINT `usuario_id` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`);

--
-- Constraints for table `tablero_movimiento`
--
ALTER TABLE `tablero_movimiento`
  ADD CONSTRAINT `tableroId` FOREIGN KEY (`tablero_id`) REFERENCES `tableros` (`id`),
  ADD CONSTRAINT `usuarioId` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
